(CNN)Hillary Clinton jabbed Donald Trump on Twitter after the GOP nominee answered a question about potential female cabinet members by naming his daughter, Ivanka, as a "popular" candidate.

Winking at a Mitt Romney gaffe from his 2012 presidential bid, Clinton wrote Thursday: "We know a guy with a binder, @realDonaldTrump. (He might not take your calls, though.)"